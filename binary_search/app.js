'use strict';

function BST(value) {
  this.value = value;
  this.left = null;
  this.right = null;
};

BST.prototype.insert = function(value) {
  if(value <= this.value) {
    if(!this.left) {
      this.left = new BST(value);
    }else {
      this.left.insert(value);
    }
  }else if(value > this.value) {
    if(!this.right) {
      this.right = new BST(value);
    }else {
      this.right.insert(value);
    }
  }
};

BST.prototype.contains = function(value){
  if(value === this.value){
    return true;
  }else if(value < this.value){
    if(!this.left){
      return false;
    }else{
      return this.left.contains(value);
    }
  }else if(value > this.value){
    if(!this.right){
      return false;
    }else{
      return this.right.contains(value);
    }
  };
  return false;
};

BST.prototype.depthFirstTraversal = function(iteratorFunction, order){
  if(order === 'pre-order') iteratorFunction(this.value);
  if(this.left) this.left.depthFirstTraversal(iteratorFunction, order);
  if(order === 'in order') iteratorFunction(this.value);
  if(this.right) this.right.depthFirstTraversal(iteratorFunction, order);
};

BST.prototype.breadthFirstTraversal = function(iteratorFunction) {
  let queue = [this];
  while(queue.length) {
    let treeNode = queue.shift();
    iteratorFunction(treeNode);
    if(treeNode.left) queue.push(treeNode.left);
    if(treeNode.right) queue.push(treeNode.right);
  }
};

//<----------get minimum value--------------->

//add min value search method to the prototype
BST.prototype.getMinValue = function() {
  if(!this.left) return this.value;
  else return this.left.getMinValue();
};

//<----------get maximum value--------------->

BST.prototype.getMaxValue = function() {
  if(!this.right) return this.value;
  else return this.right.getMaxValue();
};

let bst = new BST(50);
bst.insert(30);
bst.insert(70);
bst.insert(100);
bst.insert(60);
bst.insert(59);
bst.insert(20);
bst.insert(45);
bst.insert(35);
bst.insert(85);
bst.insert(105);
bst.insert(10);

let log = function(value){
  console.log(value);
};

function newLog(node) {
  console.log(node.value);
};

console.log(bst.depthFirstTraversal(log, 'pre-order'));
bst.breadthFirstTraversal(newLog);
console.log(bst.getMinValue());
console.log(bst.getMaxValue());

'use strict';

//Factorial (!)
//4! = 4 * 3 * 2 * 1 = 24

// 3! = 3 * 2 * 1 = 6


// this function takes a number and outputs it's factorial using recursion
function factorial(num) {

  /*recursive functions usually have 2 cases. the base case comes first, followed by the recursive case. 
    we start with the 'base case'.  this is the case which most easily satisifies the conditional.*/
  if(num === 1) {
    return num;
  }
  else {

    // now we show the recursive case.
    return num * factorial(num -1);
    
  /*if any number other than 1 is assigned to the variable what will be returned is the number 
    times the number minus 1.  once the fumber gets to 1, the recursive call stop as the base case 
    is satisified and the call stack unwinds returning all the values produced by the recursive cslls.*/
  }
};

console.log(factorial(4));
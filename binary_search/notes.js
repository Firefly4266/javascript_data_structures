'use strict';

/* this constructor will take one parameter, a value, and will have 3 properties. 
  The first property is value, which we pass the the value parameter to.  then we
  set the left and right properties to null since they do not exist yet.*/
function BST(value) {
  this.value = value;
  this.left = null;
  this.right = null;
};

// <----------------insert method---------------------->


// next we add an insert method to the prototype.  This will be a function with 1 parameter, a value.
BST.prototype.insert = function(value) {

/* Next we check to see if the value is greater <= or > the present value.  This will determine
  the path of the value that will be inserted into the BST.  Left if <=, right if > */
  if(value <= this.value) {

    // With this conditional we check to see if there is a left branch/node present.
    if(!this.left) {

      /*If no left node is present we assign a new BST with the value passed as it's argument as 
      the left branch/node of the BST. */
      this.left = new BST(value);

      /* Otherwise, if there IS a left node, we run our insert function on the left node recursively.
        this will run the above conditionals again until they are satisified. */
    }else {
      this.left.insert(value);
    }
  }else if(value > this.value) {
    if(!this.right) {
      this.right = new BST(value);
    }else {
      this.right.insert(value);
    }
  }
};

// <----------------contains method---------------------->
//Now we will add a contains method to the prototype.  this is our search
BST.prototype.contains = function(value){

  //first we check to see if the value is equal to the root node, if so we return true.
  if(value === this.value){
    return true;

    //Otherwise the value is either < or > so we move left or right accordingly. 
  }else if(value < this.value){

    /*Next we check to see if the node exist.  If it does not we return false because 
    the value does not exist in the tree. */
    if(!this.left){
      return false;

      //if the node exist we run the contains method on it to check for the value.
    }else{
      return this.left.contains(value);
    }
  }else if(value > this.value){
    /*Check to see if the node exist.  If it does not we return false because 
    the value does not exist in the tree. */

    if(!this.right){
      return false;

     //if the node exist we run the contains method on it to check for the value.
    }else{
      return this.right.contains(value);
    }
  };
  //Finally, return false if none of the conditions are satisified.
  return false;
};


// <-----------pre order depth first traversal------------------>

//add depth first traversal method to the prototype

BST.prototype.depthFirstTraversal = function(iteratorFunction, order){
/*add a second parameter to the depth first traversal method to the prototype called 'order'. This
  can be one of three values: 'in order', 'pre-order', or 'post-order'. these are string values*/

//***add a new conditional to handle pre-order traversal***
  if(order === 'pre-order') iteratorFunction(this.value);

//add the order property to the existing conditionals.
  if(this.left) this.left.depthFirstTraversal(iteratorFunction, order);

  //add a conditional here so this line will only run if we want the 'in order' traversal
  if(order === 'in order') iteratorFunction(this.value);
  if(this.right) this.right.depthFirstTraversal(iteratorFunction, order); 
};

//<---------------add breadth first search method--------------------->

/*add breadth first search method to the prototype.  This will be a function that takes
 an iterator function as its parameter*/
BST.prototype.breadthFirstTraversal = function(iteratorFunction) {

  /*next we define a queue to store nodes. we will use an array for our queue structure 
  and push the children or our current node into it for processing.  our queue will start 
  with the root node already in it. the root node will be represented by <this> */
  let queue = [this];

  /*now we implement a while loop which will run as long as there are objects in the queue */
  while(queue.length) {

    //shift the first node out of the queue and set it to a variable
    let current = queue.shift();

    //run the iterator function on the node shifted out of the queue
    iteratorFunction(current);

    /*check to see if the shifted node has children, if so push them to the queue. The 
    while loop will continue to run until the queue is empty and all its nodes are processed
    by the iterator function */
    if(current.left) queue.push(current.left);
    if(current.right) queue.push(current.right);
  }
};


let bst = new BST(50);
bst.insert(30);
bst.insert(70);
bst.insert(100);
bst.insert(60);
bst.insert(59);
bst.insert(20);
bst.insert(45);
bst.insert(35);
bst.insert(85);
bst.insert(105);
bst.insert(10);

/*finally we define our iterator function (here we call it 'log') and pass it to our method.  
  This iterator function can be ANY function needed for the task at hand.*/
let log = function(value){
  console.log(value);
};

//remember to add the second parameter
console.log(bst.depthFirstTraversal(log, 'in order'));

//<-------- for breadth first--------------->

//this iterator function will take in an entire node but only return its value property.
function newLog(node) {
  console.log(node.value);
};

bst.breadthFirstTraversal(newLog);

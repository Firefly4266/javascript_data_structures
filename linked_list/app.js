'use strict';

function Linked(){
  this.head = null;
  this.tail = null;
};

function Node(value, next, prev) {
  this.value = value;
  this.next = next;
  this.prev = prev;
};

Linked.prototype.print = function(){
  var current = this.head;
  var str = '';
  while(current.next) {
    str += current.value + ' ';
    current = current.next;
  }
  str += current.value;
  console.log('\n Linked List values are =  ' + str + '\n');
};

Linked.prototype.addToHead = function(value){  
  let newNode = new Node(value, this.head, null);  
  if(this.head) {
    this.head.prev = newNode;
  }else {
    this.tail = newNode;
  }
  this.head = newNode;
};

Linked.prototype.addToTail = function(value) {
  let newNode = new Node(value, null, this.tail);
  if(this.tail) {
    this.tail.next = newNode;
  }else {
    this.head = newNode;
  }
  this.tail = newNode;
};

Linked.prototype.removeFromHead = function() {
  if(!this.head) {
    return null;
  }
  let val = this.head.value;
  this.head = this.head.next;
  if(this.head) {
    this.head.prev = null;
  }else {
    this.tail = null;
  }
  return val;
};

Linked.prototype.removeFromTail = function() {
  if(!this.tail) {
    return null;
  }
  let val = this.tail.value;
  this.tail.next = null;
  if(this.tail) {
    this.tail.next = null;
  }else {
    this.head = null;
  }
  return val;
};

Linked.prototype.kthToLast = function(k) {
  let current = this.head;
  let kthToLast = current;
  if(! this.head || k < 1) {
    return null;
  }
  for(let i = 0; i < k - 1; i++) {
    current = current.next;
  }
  while (current.next) {
    current = current.next;
    kthToLast  = kthToLast.next;
  }
  return  kthToLast.value;
};

 
Linked.prototype.search = function(searchValue){
  let currentNode = this.head;
  while (currentNode) {
    if(searchValue === currentNode.value) {
      return currentNode.value;
    }
    currentNode = currentNode.next;
  }
  return null;
};

Linked.prototype.indexOf = function(value) {
  let indexes = [];
  let currentNode = this.head;
  let currentIndex = 0;
  while (currentNode) {
    if(currentNode.value === value) {
      indexes.push(currentIndex);
    }
    currentNode = currentNode.next;
    currentIndex++;
  }
  return indexes;
};

Linked.prototype.removeDup = function () {
  var current = this.head;
  var table = {};
  var prev = null;
  while(current){
    if(!table[current.value]){
      table[current.value] = true;
    } else {
      var next = current.next;
      current = prev;
      current.next = next;
    }
    prev = current;
    current = current.next;
  };
  // console.log(table);
  return this.head;
};

let linked = new Linked();
linked.addToHead(4);
linked.addToHead(2);
linked.addToHead(5);
linked.addToHead(22);
linked.addToHead(19);
linked.addToHead(1);
linked.addToHead(3);
linked.addToHead(22);
linked.addToTail(4);
linked.print();
linked.removeDup();
linked.print();
console.log(linked.kthToLast(4));



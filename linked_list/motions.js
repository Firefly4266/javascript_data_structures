'use strict';

function Linked(head, tail) {
  this.head = null;
  this.tail = null;
};

function Node(value, next, prev) {
  this.value = value;
  this.next = next;
  this.prev = prev;
};

Linked.prototype.addToHead = function(value) {
  let newNode = new Node(value, this.head, null);
  if(this.head) this.head.prev = newNode;
  else this.tail = newNode;
  this.head = newNode;
};

Linked.prototype.addToTail = function(value) {
  let newNode = new Node(value, null, this.tail);
  if(this.tail) this.tail.next = newNode;
  else this.head = newNode;
  this.tail = newNode;
};

Linked.prototype.removeFromHead = function() {
  if(!this.head) return null;
  let val = this.head.value;
  this.head = this.head.next;
  if(this.head) this.head.prev = null;
  else this.tail = null;
  return val;
};

Linked.prototype.removeFromTail = function() {
  if(!this.tail) return null;
  let val = this.tail.value;
  this.tail = this.tail.prev;
  if(this.tail) this.tail.next = null;
  else this.head = null;
  return val;
};

Linked.prototype.search = function(searchValue){
  let current = this.head;
  while(current) {
    if(searchValue === current.value) return current.value;
    current = current.next;
  }
  return null;
};

Linked.prototype.indexOf = function(value) {
  let indexes = [];
  let current = this.head;
  let index = 0;
  while (current) {
    if(value === current.value) indexes.push(index);
    current = current.next;
    index++;
  }
  return indexes;
};

let linked = new Linked();
linked.addToHead(300);
linked.addToHead(200);
linked.addToHead(100);
linked.addToTail(350);
linked.addToTail(450);
linked.addToTail(550);
console.log(linked.removeFromHead());
console.log(linked.removeFromTail());
console.log(linked.search(200));
console.log(linked.indexOf(45));
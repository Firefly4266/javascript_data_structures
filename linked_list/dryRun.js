'use strict';

function Link() {
  this.head = null;
  this.tail = null;
};

function Node(value, next, prev) {
  this.value = value;
  this.next = next;
  this.prev = prev;
};

Link.prototype.addToHead = function(value) {
  let newNode = new Node(value, this.head, null);
  if(this.next) this.next.prev = newNode;
  else(this.tail = newNode);
  this.head = newNode;
};

Link.prototype.addToTail = function(value) {
  let newNode = new Node(value, null, this.tail);
  if(this.tail) this.tail.next = newNode;
  else this.head = newNode;
  this.tail = newNode;
};

Link.prototype.removeHead = function() {
  if(!this.head) return null;
  let val = this.head.value;
  this.head = this.head.next;
  if(this.head) this.head.prev = null;
  else this.tail = null;
  return val;
};

Link.prototype.search = function(searchValue) {
  let currentNode = this.head;
  while(currentNode) {
    if(currentNode.value === searchValue) return currentNode.value;
    currentNode = currentNode.next;
  }
  return null;
};

Link.prototype.indexOf = function(value) {
  let indexes = [];
  let currentIndex = 0;
  let currentNode = this.head;
  while(currentNode) {
    if(currentNode.value === value) indexes.push(currentIndex);
    currentNode = currentNode.next;
    currentIndex++;
  }
   return indexes;
}

let newLinked = new Link;
newLinked.addToHead(100);
newLinked.addToTail(205);
newLinked.addToTail(210);
newLinked.addToTail(215);

console.log(newLinked.removeHead()); 
console.log(newLinked.search(205));
console.log(newLinked.indexOf(210));